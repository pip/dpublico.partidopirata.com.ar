all: build

build-images:
	mkdir -p dist/images
	
	svgo src/images/ddp.svg -o dist/images/ddp.optim.svg
	svgo src/images/ddp_flayer.svg -o dist/images/ddp_flayer.optim.svg
	svgo src/images/tiket.svg -o dist/images/tiket.optim.svg
	
	cp src/images/*.webp dist/images/

build-html:
	node build_html.js

copy-all:
	mkdir -p dist/
	cp -r src/* dist/

compress:
	gzip -fk dist/*.html
	gzip -fk dist/css/*.css

build: copy-all build-images build-html compress
