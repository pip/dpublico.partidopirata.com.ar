const fs = require('fs')

const readSvg = svg => fs.readFileSync(`./dist/images/${svg}.optim.svg`).toString()
let svgs = {
    ddp: readSvg('ddp'),
    ddp_flayer: readSvg('ddp_flayer'),
    tiket: readSvg('tiket'),
}

const htmls = fs.readdirSync('./src').filter(s => s.endsWith('.html'))

for (const file of htmls) {
    const string = fs.readFileSync('./src/' + file).toString()

    let output = []

    let arr = string.split('\n')
    arr = arr.map((line, index) => {
        const matches = line.match(/<!--INYECTAR:(\w+) (.+)-->/)
        if (matches) {
            let [, svg, label] = matches
            svg = svgs[svg]
            svg = svg.replace('<svg', '<svg role="img" aria-labelledby="title"')
            svg = svg.replace('</svg>', `<title id="title">${label}</title></svg>`)
            line = svg
        }
        return line
    })

    fs.writeFileSync('./dist/' + file, arr.join('\n'))

}
